import datetime
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_protect
from forum.models import User, Question, Answer


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def fake_questions(count=30, tag=None):
    questions = []
    for i in range(1, count):
        questions.append({
            'title': 'title ' + str(i),
            'pk': i,
            'text': 'text' + str(i),
            'is_active': True,
            'tags': [{
                'title': tag if tag is not None else 'tag' + str(i),
                'get_absolute_url': '#'}],
            'created_date': datetime.datetime.now(),
            'author': {
                'image_path': '/static/img/default.png',
                'username': 'User' + str(i),
                'pk': str(i),
            },
            'votes': {'count': str(i), },
        })
    return questions


def fake_answers():
    answers = []
    for i in range(1, 5):
        answers.append({
            'title': 'title ' + str(i),
            'pk': i,
            'text': 'text' + str(i),
            'created_date': datetime.datetime.now(),
            'question': {'author': {'pk': str(i)}},
            'author': {
                'image_path': '/static/img/default.png',
                'username': 'User' + str(i),
                'pk': str(i),
            },
            'votes': {'count': str(i), },
        })
    return answers


def current_profile(user):
    if user.is_authenticated:
        return User.objects.get(pk=user.pk)
    return None


def paginate(objects_list, request, post_num=25):
    paginator = Paginator(objects_list, post_num)
    page = request.GET.get('page')
    try:
        objects_page = paginator.page(page)
    except PageNotAnInteger:
        objects_page = paginator.page(1)
    except EmptyPage:
        objects_page = paginator.page(paginator.num_pages)

    return objects_page


def post_list(request):
    questions = Question.objects.get_feed()
    questions_part = paginate(questions, request)
    print(request)
    print(request.user)
    print(questions_part[0])
    return render(request=request, template_name='forum/index.html',
                  context={'questions': questions_part, 'profile': current_profile(request.user)})


def hot(request):
    questions = Question.objects.get_hot()
    questions_part = paginate(questions, request)
    is_hot = True
    return render(request=request, template_name='forum/index.html',
                  context={'questions': questions_part, 'profile': current_profile(request.user), 'status': is_hot})


def by_tag(request, tag):
    questions = Question.objects.get_tag(tag)
    questions_part = paginate(questions, request)
    return render(request=request, template_name='forum/tags.html',
                  context={'questions': questions_part, 'tag': tag, 'profile': current_profile(request.user)})


def question_detail(request, pk):
    question = get_object_or_404(Question, pk=pk)
    # answers = fake_answers()
    answers = Answer.objects.filter(question=question)
    return render(request=request, template_name='forum/question.html',
                  context={'question': question, 'answers': answers, 'profile': current_profile(request.user)})


@csrf_protect
def login(request):
    return render(request=request, template_name='register/login.html')


@csrf_protect
def signup(request):
    return render(request=request, template_name='register/signup.html')


def question_new(request):
    return render(request=request, template_name='forum/ask.html')

