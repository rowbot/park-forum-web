import os
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.db import models, IntegrityError
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.contenttypes.models import ContentType


class User(AbstractUser):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    username = models.CharField(max_length=40, unique=True, db_index=True, null=True)
    upload = models.ImageField(upload_to='uploads/%Y/%m/%d/', default='default.png')

    def image_path(self):
        if self.upload:
            return self.upload.url
        else:
            return os.path.join(settings.MEDIA_URL, "default.png")


class Tag(models.Model):
    title = models.CharField(max_length=200, unique=True, verbose_name='tag name')
    question_num = models.IntegerField(default=0, verbose_name='number of Questions')

    def get_absolute_url(self):
        return reverse("tag", kwargs={'tag': self.title})

    def __str__(self):
        return self.title


class QuestionManager(models.Manager):
    def get_hot(self):
        return self.order_by('-rating')

    def get_feed(self):
        return self.order_by('-created_date')

    def get_tag(self, tag):
        return self.filter(tags__title=tag)


class Like(models.Model):
    VALUES = (
        ('UP', 1),
        ('DOWN', -1),
    )
    user = models.ForeignKey(User, related_name='liker', on_delete=models.CASCADE)
    value = models.SmallIntegerField(default=0, choices=VALUES)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class Question(models.Model):
    author = models.ForeignKey(to=User, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=200)
    text = models.TextField()
    objects = QuestionManager()
    tags = models.ManyToManyField(to=Tag, related_name='questions')
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True, verbose_name="is_active")
    rating = models.IntegerField(default=0, verbose_name='rating')
    votes = GenericRelation(Like, related_query_name='questions', default=0)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def get_answers(self, order_by=None):
        return self.answer_set.get_feed(order_by)

    def get_user(self):
        return User.objects.get(user=self.author)

    def get_absolute_url(self):
        return reverse('question_detail', kwargs={'pk': self.pk})

    def get_like_url(self):
        return reverse('like-toggle', kwargs={'pk': self.pk})

    def get_api_like_url(self):
        return reverse('like-api-toggle', kwargs={'pk': self.pk})

    def add_tags(self, tags):
        for tag in tags:
            if tag.pk:
                self.tags.add(tag)
            else:
                try:
                    tag.save()
                except IntegrityError:
                    first = Tag.objects.filter(title=tag.title).first()
                    self.tags.add(first)
                else:
                    self.tags.add(tag)

    def __str__(self):
        return self.title


class AnswerManager(models.Manager):
    def get_feed(self, order_by=None):
        order_by = order_by or ('-votes', 'created_date')
        return self.filter(is_active=True).order_by(*order_by)


class Answer(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    text = models.TextField(null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def get_like_url(self):
        return reverse('choose_answer', kwargs={'pk': self.question.pk})

    def get_api_like_url(self):
        return reverse('answer-api-toggle', kwargs={'pk': self.question.pk})

    def __str__(self):
        return self.text
